#!/bin/bash

if [ $# -lt 1 ];then 
    numbers=10;
else
    numbers=$1;
fi;

# compilation
mpic++ --prefix /usr/local/share/OpenMPI -o es es.cpp

# create input file with random inputs
dd if=/dev/random bs=1 count="$numbers" of=numbers 2> /dev/null

# # of processors must be greater by 1 than number of inputs
((numbers++))
	
# run
mpirun --prefix /usr/local/share/OpenMPI -np $numbers es

# clean up
rm -f es numbers
