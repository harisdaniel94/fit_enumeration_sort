/**
 * Project: PRL 2 - Enumeration sort 
 *  Author: Daniel Haris
 *   Login: xharis00
*/

#include <mpi.h>
#include <stdio.h>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>

#define TAG_X 1
#define TAG_Y 2
#define TAG_Z 3
#define TAG_OUT 4

using namespace std;

struct procRegs {
    unsigned int C, Xi, Yi;
    unsigned char X, Y, Z;
};

// set to true for measuring sorting time
bool runExperiments = false;

/**
 * It is example code from: http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
 * @param time from
 * @param time to
 * @return difference
 */
timespec diff(timespec start, timespec end) {
    timespec temp;
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

/**
 * @brief Function reads bytes from a file
 * @param fileName name of the file
 * @return vector of random numbers
 */
vector<unsigned char> getInputSequence(string fileName) {
    
    fstream f;
    vector<unsigned char> inputSequence;
    f.open(fileName.c_str(), ios::in);
    for(int num = f.get(); f.good() && num != char_traits<char>::eof(); num = f.get())
        inputSequence.push_back((unsigned char) num);
    f.close();
    return inputSequence;
}

/**
 * @brief Main function
 */
int main(int argc, char *argv[]) {
    
    /* define variables */
    int numProc;
    int myid;
    MPI_Status stat;
    timespec time1, time2;

    /* initialize MPI */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numProc);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);

    if (myid == numProc-1) {

        // load & print input numbers
        string fileName = "numbers";
        vector<unsigned char> inputSequence = getInputSequence(fileName);
        for (int i = 0; i < inputSequence.size(); i++)
            cout << (int) inputSequence.at(i) << " ";
        cout << endl;
        
        // check if number of processors dont match expected value
        if (numProc != inputSequence.size()+1) {
            cerr << "Invalid number of processors: # of processors == # of data + 1." << endl;
            return -1;
        }

        // >> start measuring execution time
        if (runExperiments) {
            clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
        }
        
        // MPI_SEND - send data to processors
        for (int i = 0; i < numProc-1; i++) {
            int data_in[2] = { inputSequence.at(i), i+1 };
            MPI_Send(data_in, 2, MPI_INTEGER, i, TAG_X, MPI_COMM_WORLD);
            MPI_Send(data_in, 2, MPI_INTEGER, 0, TAG_Y, MPI_COMM_WORLD);
        }

        // MPI_RECV - recieve processor results
        unsigned char *outputSequence = new unsigned char[inputSequence.size()];
        for (int i = 0; i < inputSequence.size(); i++) {
            int data_out[2];
            MPI_Recv(data_out, 2, MPI_INTEGER, MPI_ANY_SOURCE, TAG_OUT, MPI_COMM_WORLD, &stat);
            unsigned char val = (unsigned char) data_out[0];
            int ind = (int) data_out[1];
            outputSequence[ind] = val;
        }
        
        // >> stop measuring
        if (runExperiments) 
            clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
        
        // print out sorted sequence
        for (int i = 0; i < inputSequence.size(); i++)
            cout << (int) outputSequence[i] << endl;

        // >> display execution time
        if (runExperiments)
            cout << diff(time1, time2).tv_sec << "s : " << diff(time1, time2).tv_nsec << "ns" << endl;

        // deallocate memory
        delete[] outputSequence;

    } else {
        
        // define variables
        procRegs regs;
        int data_rec[2];
        regs.C = 0;

        MPI_Recv(data_rec, 2, MPI_INTEGER, numProc-1, TAG_X, MPI_COMM_WORLD, &stat);
        regs.X = (unsigned char) data_rec[0];
        regs.Xi = (int) data_rec[1];
        
        for (int i = 0; i < numProc-1; i++) {
            MPI_Recv(data_rec, 2, MPI_INTEGER, !myid?numProc-1:myid-1, TAG_Y, MPI_COMM_WORLD, &stat);
            regs.Y = (unsigned char) data_rec[0];
            regs.Yi = (int) data_rec[1];
            if ((regs.X > regs.Y) || (regs.X == regs.Y && regs.Xi > regs.Yi))
                regs.C++;
            if (myid < (numProc - 1))
                MPI_Send(data_rec, 2, MPI_INTEGER, myid+1, TAG_Y, MPI_COMM_WORLD);
        }
        
        MPI_Send(&regs.X, 1, MPI_BYTE, regs.C, TAG_Z, MPI_COMM_WORLD);
        MPI_Recv(&regs.Z, 1, MPI_BYTE, MPI_ANY_SOURCE, TAG_Z, MPI_COMM_WORLD, &stat);
        
        int result[2] = { regs.Z, myid };
        MPI_Send(result, 2, MPI_INTEGER, numProc-1, TAG_OUT, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}
